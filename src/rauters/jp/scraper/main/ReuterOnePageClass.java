package rauters.jp.scraper.main;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import library.java.FileClass;

public class ReuterOnePageClass implements KeyInterface{
	private List<String> titles = new ArrayList<String>();
	private List<String> details = new ArrayList<String>();
	private List<String> dateList = new ArrayList<String>();
	
	
	public ReuterOnePageClass() {
	}
	
	public void scrape(String url){
		try {
			Document document = Jsoup.connect(url).get();
			Elements elements = document.select("#moreSectionNews > div.module > div.moduleBody > div.feature > h3.topStory2 > a"); 
			for (Element element : elements) {
				titles.add(element.text());
			}
			
			elements = document.select("#moreSectionNews > div.module > div.moduleBody > div.feature > p");
			for (Element element : elements) {
				details.add(element.text());
			}
			elements = document.select("#moreSectionNews > div.module > div.moduleBody > div.feature > div.relatedInfo > span.timestamp");
			for (Element element : elements) {
				dateList.add(element.text());
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void save(Calendar cal){
		String[][] data = new String[titles.size()][5];
		
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
		for(int rowCount = 0; rowCount < data.length; rowCount++) {
			int countFromEnd = (data.length - 1) - rowCount;
			data[rowCount][0] = year + "/" + month + "/" + day;
			data[rowCount][1] = getDayOfWeekStr(dayWeek);
			data[rowCount][2] = dateList.get(countFromEnd);
			data[rowCount][3] = titles.get(countFromEnd);
			data[rowCount][4] = details.get(countFromEnd);
		}
		FileClass.appendAsCsv(data, WORKSPACE + MAIN_FILE);
		FileClass.append(cal.getTime().toString(), WORKSPACE + LOG_FILE);
	}
	
	private String getDayOfWeekStr(int dayOfWeekNum){
		if(dayOfWeekNum == Calendar.SUNDAY) return "日";
		if(dayOfWeekNum == Calendar.MONDAY) return "月";
		if(dayOfWeekNum == Calendar.TUESDAY) return "火";
		if(dayOfWeekNum == Calendar.WEDNESDAY) return "水";
		if(dayOfWeekNum == Calendar.THURSDAY) return "木";
		if(dayOfWeekNum == Calendar.FRIDAY) return "金";
		if(dayOfWeekNum == Calendar.SATURDAY) return "土";
		return "error";
	}
	
	
}


