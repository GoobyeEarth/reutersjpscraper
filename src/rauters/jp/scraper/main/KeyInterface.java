package rauters.jp.scraper.main;

public interface KeyInterface {
	String WORKSPACE = "/Users/aberintaro/Documents/workspace/ReutersJpScraper/";
	String INITIAL_CALENDAR = "2008/7/2";
	String LOG_FILE = "log.txt";
	String MAIN_FILE = "reuter_jp_archive.txt";
	String TITLE = "Reuters.jp: scrape forex archive";
	int SCRAPE_INTERVAL = 3;
}