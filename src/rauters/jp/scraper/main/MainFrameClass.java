package rauters.jp.scraper.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.NoRouteToHostException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
public class MainFrameClass implements KeyInterface{
	private JButton button;
	private GridBagConstraints mainGridConstraints;
	
	private JPanel mainGridBugPanel;
	private JFrame frame;
	
	public RautersJpScraperClass reutersJp = new RautersJpScraperClass();
	
	
	public void view(){
		mainGridBugPanel = mainGridBugPanel();
		mainGridConstraints = new GridBagConstraints();
		mainGridConstraints.fill = GridBagConstraints.BOTH;
		
		button = new JButton("start");
		mainGridConstraints.gridx = 0;
		mainGridConstraints.gridy = 0;
		mainGridConstraints.weightx = 1;
		mainGridConstraints.weighty = 1;
		button.setFont(new Font("Arial", Font.BOLD, 32));
		mainGridBugPanel.add(button, mainGridConstraints);
		
		frame = new JFrame();
		frame.setSize(new Dimension(300, 150));
		frame.setTitle(TITLE);
		frame.setContentPane(mainGridBugPanel);
		frame.setVisible(true);
		
	}
	
	public JPanel mainGridBugPanel(){
		JPanel jPanel = new JPanel();
		GridBagLayout gridLayout = new GridBagLayout();
		jPanel.setLayout(gridLayout);
		
		return jPanel;
	}
	
	public JScrollPane setScrollPane(JPanel jPanel){
		JScrollPane scrollPane = new JScrollPane(jPanel,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setMinimumSize(new Dimension(100, 200));
		scrollPane.setSize(new Dimension(100, 200));
		return scrollPane;
	}
	
	public JPanel scrollGridBugPanel(){
		JPanel jPanel = new JPanel();
		GridBagLayout gridLayout = new GridBagLayout();
		jPanel.setLayout(gridLayout);
		return jPanel;
	}
	
	public void listener(){
		ActionListener buttonListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(reutersJp.isWorking()){
					reutersJp.stop();
					button.setText("start");
				}
				else{
					reutersJp.runBackground();
					button.setText("stop");
				}
			}
		};
		button.addActionListener(buttonListener);
		
	}
	
	
}
