package rauters.jp.scraper.main;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import library.java.FileClass;

public class RautersJpScraperClass implements KeyInterface{
	private Calendar startCal;
	private Calendar endCal;
	
	private boolean orderToWork = false;
	
	private MyThread myThread = new MyThread();
	
	public RautersJpScraperClass(){
		setCalenders();
		
	}
	
	private void setCalenders(){

		startCal = Calendar.getInstance();
		String[] data = FileClass.load(WORKSPACE + LOG_FILE);
		if(data == null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd", Locale.US);
			
			try {
				startCal.setTime(sdf.parse(INITIAL_CALENDAR));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
			
			try {
				System.out.println("start from: " + data[data.length - 1]);
				startCal.setTime(sdf.parse(data[data.length - 1]));
				
				startCal.add(Calendar.DAY_OF_MONTH, 1);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		endCal = Calendar.getInstance();
		
	}
	
	private void execute(){
		
		if(orderToWork == true) {
			System.out.println("scraping is working!");
			return ;
		}
		else{
			orderToWork = true;
			System.out.println("start to work");
		}
		Calendar proceedingCal = startCal;
		
		while(proceedingCal.getTimeInMillis() < endCal.getTimeInMillis() && orderToWork){
			
			executeOnePage(proceedingCal);
			System.out.println(proceedingCal.getTime().toString());
			
			
			//to avoid attacking server
			try {
				Thread.sleep(SCRAPE_INTERVAL * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			proceedingCal.add(Calendar.DAY_OF_MONTH, 1);
		}
		
		if(orderToWork == true){
			System.out.println("update completed");
		}
		else{
			System.out.println("safely stoped");
		}
	}
	
	private void executeOnePage(Calendar cal){
		String url = getUrl(cal);
		ReuterOnePageClass onePage = new ReuterOnePageClass();
		onePage.scrape(url);
		onePage.save(cal);
		
	}
	
	private String getUrl(Calendar cal){
		final String BASE_URL = "http://jp.reuters.com/news/archive/forexMarketOutlook?date=";
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		String url = BASE_URL;
		url += new DecimalFormat("00").format(month);
		url += new DecimalFormat("00").format(day);
		url += new DecimalFormat("00").format(year);
		
		
		
		return url;
	}
	
	public boolean isWorking(){
		return orderToWork;
	}
	
	public void runBackground(){
		myThread = new MyThread();
		myThread.start();
	}
	/**
	 *  safely stop
	 */
	public void stop(){
		orderToWork = false;
	}
	
	private class MyThread extends Thread{
		public void run(){
			execute();
		}
	}

}
